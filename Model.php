<?php
abstract class Model{
  //Declaramos variables globales
  protected $user;
  protected $pass;
  protected $db;
  protected $host;
  protected $port;
  protected $conn;
  protected static $all;
  protected $table;
  //Constructor donde asignamos el valor correspondiente a cada variable
  function __construct(){
    $this->user="root";
    $this->pass="";
    $this->db="libreria";
    $this->host="127.0.0.1";
    $this->port=3306;
  }
  //Funcion donde traemos todas las filas de la tabla
  public function all(){
    return $this->executeQuery("select * from ".$this->table);
  }

  public function delete(){
    return $this->executeQuery("delete * from ".$this->table);
  }

  //Funcion donde traemos las filas con el campo que corresponde al valor dado
  public function where($field,$value){
    return $this->executeQuery("select * from ".$this->table." where {$field} = {$value}");
  }
  //Funcion donde ejecutamos la sentencia SQL formada por otra funcion
  public function executeQuery($query){
    //Se abre la conexion;
    $this->openConnection();
    //Se obtiene el resultset de la consulta
    $result = $this->conn->query($query);

    //Obtenenos todas las filas y las ponemos en data
    $data = [];
    while($row = $result->fetch_assoc()){ array_push($data,$row); }
    //Cerramos la conexion
    $this->closeConection();
    //Regresamos los datos
    return $data;
  }
  //Abrimos la conexion
  public function openConnection(){
    $this->conn = new mysqli($this->host, $this->user, $this->pass, $this->db,$this->port);
    $this->conn->set_charset('utf8');
  }
  //Cerramos la conexion
  public function closeConection(){
    mysqli_close($this->conn);
  }
}
