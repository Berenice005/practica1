﻿<?php
//Incluimos el archivo Model.php para contar con la clase model
include "Model.php";
include "Libro.php";
include "Autor.php";
//Creamos un objeto de la clase model
$libro = new Libro;
$autor = new Autor;
//Obetenemos todos los libros de la base de datos
$libros  = $libro->all();
$autores = $autor->all();
//Nuevamene un cometario
?>
<!-- Dibujamos la tabla con los datos de los libros -->
<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>Nombre</th>
      <th>Descripcion</th>
      <th>Autor</th>
      <th>Año</th>
    </tr>
  </thead>
  <tbody>
  <?php foreach($libros as $libro):?>
    <tr>
      <?php foreach ($libro as $value):?>
        <td><?=$value?></td>
      <?php endforeach;?>
    </tr>
  <?php endforeach;?>
  </tbody>
</table>
<!-- autores -->
<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>Nombre</th>
      <th>Apellido</th>
      <th>Edad</th>
      <th>Nacionalidad</th>
    </tr>
  </thead>
  <tbody>
  <?php foreach($autores as $autor):?>
    <tr>
      <?php foreach ($autor as $value):?>
        <td><?=$value?></td>
      <?php endforeach;?>
    </tr>
  <?php endforeach;?>
  </tbody>
</table>
